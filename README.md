# AIAD lab. 15/15 - Wykorzystanie obliczeń równoległych do przetwarzania danych w Pythonie na przykładzie biblioteki Pandas

Rozwiązania poniższych zadań (takie lub podobne same zadania jak na Moodle do przedmiotu)
proszę umieszczać w osobnych katalogach, nazwanych odpowiednio `zad_N.1/`, `zad_D.2/` itd.

Jeśli w katalogu z rozwiązaniem danego zadania znajduje się więcej niż jeden skrypt Python,
to proszę dodać plik `README.md` (w formacie [Markdown][1], dialekt [GLFM][2]) lub `README` (plik tekstowy),
zawierający informację o tym jak projekt uruchamiać.

**Uwaga:** aby uniknąć konfliktów każdy powinien stworzyć **_nową gałąź_**,
o takiej samej nazwie jak własna nazwa użytkownika w systemie ($USERNAME),
lub nazwaną według szablonu `Imie_Nazwisko` (bez używania polskich liter),
i się na tą gałąź przełączyć (i na niej wprowadzać zmiany).
Na przykład jeśli moja nazwa użytkownika to `jnareb`, to należy stworzyć
gałąź `jnareb` np. za pomocą
```
$ git switch --create jnareb
```
Rozwiązanie należy zatwierdzić tworząc commit, a następnie opublikować (operacja push).

[1]: https://www.markdownguide.org/getting-started/ "Markdown Guide | Getting Started"
[2]: https://docs.gitlab.com/ee/user/markdown.html "GitLab Flavored Markdown (GLFM)"


-----
